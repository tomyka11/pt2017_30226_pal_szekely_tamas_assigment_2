import java.util.*;
public class Customer {

	private long cID;
	private Date arrivingTime;
	private Date finishTime;
	private int serviceTime; 
	
	public Customer(long cID,Date aT,Date fT)
	{
		this.cID=cID;
		this.arrivingTime=aT;
		this.finishTime=fT;
	}
	
	public long getID()
	{
		return this.cID;
	}
	public Date getaT()
	{
		return this.arrivingTime;
	}
	public Date getfT()
	{
		return this.finishTime;
	}
	public void setsT(int sT){
		this.serviceTime=sT;
	}
	public int getsT()
	{
		return this.serviceTime;
	}
	public String toString()
	{
		return (Long.toString(cID)+" "+arrivingTime.toString()+" "+finishTime.toString()+ " "+serviceTime);
	}
}
