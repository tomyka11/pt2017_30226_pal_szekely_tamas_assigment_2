import java.awt.*;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.BoxLayout;
import javax.swing.JButton;


public class GUI extends JFrame {

	public JTextField arrmintime=new JTextField(5);
	public JTextField arrtimemax=new JTextField(5);
	public JTextField servicemin=new JTextField(5);
	public JTextField servicemax=new JTextField(5);
	public JTextField nrq=new JTextField(5);
	public JTextField simintmin=new JTextField(5);
    public JButton start=new JButton("SIMULATE");
    

	
	public GUI(){
		
		
		
		
		JPanel phoriz=new JPanel();
		phoriz.setLayout(new BoxLayout(phoriz,BoxLayout.Y_AXIS));
		
		
		JPanel panel1=new JPanel();
		panel1.setLayout(new FlowLayout());
		JPanel panel2=new JPanel();
		panel2.setLayout(new FlowLayout());
		JPanel panel3=new JPanel();
		panel3.setLayout(new FlowLayout());
		JPanel panel4=new JPanel();
		panel4.setLayout(new FlowLayout());
		JPanel panel5=new JPanel();
		panel5.setLayout(new FlowLayout());
		JPanel panel6=new JPanel();
		panel6.setLayout(new FlowLayout());
		JPanel panel7=new JPanel();
		panel7.setLayout(new FlowLayout());
		JPanel panel8=new JPanel();
		panel8.setLayout(new FlowLayout());
		JPanel panel9=new JPanel();
		panel9.setLayout(new FlowLayout());
		
		
		phoriz.add(panel1);
		phoriz.add(panel2);
		phoriz.add(panel3);
		phoriz.add(panel4);
		phoriz.add(panel5);
		phoriz.add(panel6);
		phoriz.add(panel7);
		phoriz.add(panel8);
		phoriz.add(panel9);
		
		panel1.add(new JLabel("SIMULATION INPUT"));
		panel2.add(new JLabel("Minimum and maximum interval of arriving time between customers"));
		panel3.add(new JLabel("Min: "));
		panel3.add(arrmintime);
		panel3.add(new JLabel("Max: "));
		panel3.add(arrtimemax);
		panel4.add(new JLabel("Minimum and maximum service time"));
		panel5.add(new JLabel("Min: "));
		panel5.add(servicemin);
		panel5.add(new JLabel("Max: "));
		panel5.add(servicemax);
		panel6.add(new JLabel("Number of Queues"));
		panel6.add(nrq);
		panel7 .add(new JLabel("Simulation interval"));
		panel8.add(new JLabel("Interval of Simulation(<60sec): "));
		panel8.add(simintmin);
		panel9.add(start);
		
		
		  this.setContentPane(phoriz);
		    this.pack();
		    this.setTitle("Simulate");
		    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public String getarrivemin() {
        return arrmintime.getText();
    }
	public String getarrivemax() {
        return arrtimemax.getText();
    }
	public String getservicemin() {
        return servicemin.getText();
    }
	public String getservicemax() {
        return servicemax.getText();
    }
	public String getnrqueue() {
        return nrq.getText();
    }
	public String getsimintmin() {
        return simintmin.getText();
    }
	public void setarrivemin(String minarr){
		arrmintime.setText(minarr);
	}
	
	public void setarrivemax(String arrmax) {
       arrtimemax.setText(arrmax);
    }
	public void setservicemin(String svmin) {
	       servicemin.setText(svmin);
	    }
	public void setservicemax(String svmax) {
	       servicemax.setText(svmax);
	    }
	public void setnrqu(String nrq1) {
	       nrq.setText(nrq1);
	    }
	public void setsimintmin(String ssmi){
		simintmin.setText(ssmi);
	}
	
	public void addStartListener(ActionListener mal) {
       start.addActionListener(mal);
    }

	
	
    
}
